package tz

import com.github.javafaker.Faker
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@EnableAutoConfiguration
@ComponentScan
@Configuration
@RestController
class App extends WebSecurityConfigurerAdapter {
    @Bean
    Faker faker() { new Faker() }

    @RequestMapping("/fakedata")
    def fake() {
        def res = []
        0.upto(100) {
            res << ['name'     : faker().name().fullName(),
                    'price'    : faker().numerify('##,##'),
                    'change'   : faker().numerify('##,##'),
                    'pctChange': faker().numerify('##,##')]
        }
        res
    }

    @Autowired
    def configureGlobal(AuthenticationManagerBuilder auth) {
        auth.inMemoryAuthentication().withUser("admin").password("nimda").roles("ADMIN");
    }

    @Override
    void configure(HttpSecurity http) {
        http
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login.html")
                .passwordParameter("pass")
                .usernameParameter("user")
                .defaultSuccessUrl("/index.html")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/css**")
                .permitAll()
                .and()
                .csrf()
                .disable()
                .logout()
                .logoutSuccessUrl("/index.html")
                .and()
                .httpBasic();
    }

    static main(args) { SpringApplication.run App.class }


}
