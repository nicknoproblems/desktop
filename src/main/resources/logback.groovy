import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.FileAppender

scan("30 seconds")

def ts = timestamp("yyyy-MM-dd")

appender("FILE", FileAppender) {
    file = "tz${ts}.log"
    append = true
    encoder(PatternLayoutEncoder) {
        pattern = "%level %logger - %msg%n"
    }
}

appender("CONSOLE",ConsoleAppender){
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %-4relative %-5level %logger{35} - %msg%n"
    }
}

root(INFO,["FILE","CONSOLE"])
//logger('org.springframework.jdbc.core.JdbcTemplate',TRACE,["CONSOLE"])
